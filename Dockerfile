FROM ubuntu:20.04

# Set Env
ARG USER_PASSWD
ENV DEBIAN_FRONTEND noninteractive
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Install Base Dependencies Curl -nGit
RUN apt-get update && apt-get install -y -q --no-install-recommends \
    apt-transport-https \
    build-essential \
    ca-certificates \
    curl \
    git \
    libssl-dev \
    lsb-release \
    software-properties-common \
    sudo \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install NVM
RUN mkdir -p /usr/local/nvm
ENV NVM_DIR /usr/local/nvm
SHELL ["/bin/bash", "--login", "-c", "-o", "pipefail"]
RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.39.1/install.sh | bash
# shellcheck source=/.bashrc
RUN . ~/.bashrc

# Install Node
ENV NODE_VERSION 16.14.2
# shellcheck source=/usr/local/nvm/nvm.sh
RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use $NODE_VERSION

# Install NPM
RUN apt-get update && apt-get install -y --no-install-recommends npm \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*
ENV NPM_CONFIG_PREFIX=/home/ubuntu/.npm-global
ENV PATH=$PATH:/home/ubuntu/.npm-global/bin

RUN npm install -g npm@8.5.0   
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH


# Install Yarn
RUN apt-get update && apt-get install -y --no-install-recommends gnupg2 \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -y && apt-get install -y --no-install-recommends yarn \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

# Install Git
RUN apt-get update && apt-get install -y --no-install-recommends git \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

# Install Dockers
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
RUN apt-get update && apt-get install -y --no-install-recommends docker-ce \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*
RUN /etc/init.d/docker start
RUN /lib/systemd/systemd-sysv-install enable docker

# Install Docker Compose
RUN mkdir -p /usr/local/lib/docker/cli-plugins
RUN curl -SL https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-linux-x86_64 -o /usr/local/lib/docker/cli-plugins/docker-compose
RUN chmod +x /usr/local/lib/docker/cli-plugins/docker-compose

# Verify Installs
RUN node -v
RUN npm --version
RUN curl --version
RUN yarn --version
RUN git --version
RUN docker -v
RUN docker compose version

#Copy Env Vars Script
COPY setup_env.sh /home/ubuntu/
RUN sed -i -e 's/\r//g' /home/ubuntu/setup_env.sh

# Set User
RUN useradd -ms /bin/bash -G sudo,docker ubuntu
RUN echo "ubuntu:$USER_PASSWD" | chpasswd
USER ubuntu
WORKDIR /home/ubuntu