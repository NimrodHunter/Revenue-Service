#!/bin/bash

echo USER_PASSWD=$USER_PASSWD >> .env
echo PORT=$PORT >> .env
echo NETWORK=$NETWORK >> .env
echo INFURA_API_KEY=$INFURA_API_KEY >> .env
echo MNEMONIC=$MNEMONIC >> .env
echo FACTORY=$FACTORY >> .env
echo REWARD=$REWARD >> .env
echo ANCHOR_NFT=$ANCHOR_NFT >> .env
echo AAVE_NFT=$AAVE_NFT >> .env
echo SPELL_NFT=$SPELL_NFT >> .env
echo UNISWAP_NFT=$UNISWAP_NFT >> .env
echo YEARN_NFT=$YEARN_NFT >> .env