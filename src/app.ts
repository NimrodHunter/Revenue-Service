import 'dotenv/config';

import express, { Request, Response } from 'express';
import cors from 'cors';
import nft from './routes/nft';
import merkle from './routes/merkle';
import claim from './routes/claim';

const app = express();

app.use(cors({
    origin: "*"
}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// service up
app.get("/status", (req, res) => {
    res.status(200).send({ status: "running..." });
});
// routes
app.use(nft);
app.use(merkle);
app.use(claim);

//default
app.use((req: Request, res: Response)=>{
    res.status(404).send('<h1> Page not found </h1>');
});

 //listeners
 // Server port
const HTTP_PORT = process.env.PORT || 3000 
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Service running on port %PORT%".replace("%PORT%", HTTP_PORT.toString()))
});