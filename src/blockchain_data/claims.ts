import { ethers } from "ethers";
import  { provider } from '../utils/provider';

import factoryAbi from '../abis/factory.json';
import rewardAbi from '../abis/reward.json';
import tokenAbi from '../abis/token.json';

import factoryAddress from '../utils/revenueFactory';
import rewardImplementation from '../utils/reward';

import { Token, Claim, NFTClaims } from '../types/claim';

import { addressNFTs, nftData } from './nfts';

const projectClaims = async (project: string, id: number) => {
    const notFound = new Error('not found');
    let claims: Claim[] | Error = [];
    const factoryContract = new ethers.Contract(factoryAddress, factoryAbi, provider);
    let rewardContract = new ethers.Contract(rewardImplementation, rewardAbi, provider);
    try {
        const numOfRewards = await factoryContract.claimables();
        if (numOfRewards > 0) {
            for (let i = 1; i <= numOfRewards; i++) {
                const claimables = await factoryContract.revenuesAddress(i);
                rewardContract = rewardContract.attach(claimables);
                const nftAddress = await rewardContract.nft();
                if (nftAddress.localeCompare(project) === 0) {
                    const claimed = await rewardContract.claimed(id);
                    const nftInfo = await nftData(project, id);
                    if (nftInfo instanceof Error) return nftInfo;
                    let blockNumber = await rewardContract.blockNumber();
                    blockNumber = blockNumber.toNumber();
                    const block = await provider.getBlock(blockNumber);
                    if (!claimed && parseInt(nftInfo.createAt) <= block.timestamp) {
                        const rewardToken = await rewardContract.rewardToken();
                        const tokenContract = new ethers.Contract(rewardToken, tokenAbi, provider);
                        const symbol = await tokenContract.symbol();
                        const revenue = await rewardContract.revenue();
                        const token: Token = {
                            address: rewardToken.address,
                            symbol: symbol,
                            revenue: revenue.toString()
                        }
                        const claim: Claim = {
                            contract: claimables,
                            blockNumber: blockNumber,
                            token: token
                        }
                        claims.push(claim);
                    }
                }
            }
        }
    } catch (e) {
        if (e instanceof Error) {
            console.log(e)
            claims = e;
        }
    }
    if (Array.isArray(claims) && claims.length === 0) {
        claims = notFound;
    }
    return claims;
}

const userClaims = async (user: string) => {
    const notFound = new Error('not found');
    let totalClaims: NFTClaims[] | Error = [];
    try {
        const nfts = await addressNFTs(user);
        if (nfts instanceof Error) return nfts;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        await Promise.all(nfts.map(async (nft: any) => {
            const claims = await projectClaims(nft.project, parseInt(nft.nft[0].id))
            if (claims instanceof Error || totalClaims instanceof Error) return claims;
            const newProjectClaims: NFTClaims = {
                project: nft.project,
                nftId: nft.nft[0].id,
                revenues: claims
            }
            totalClaims.push(newProjectClaims);
        }));
    } catch (e) {
        if (e instanceof Error) {
            console.log(e)
            totalClaims = e;
        }
    }
    if (Array.isArray(totalClaims) && totalClaims.length === 0) {
        totalClaims = notFound;
    }
    return totalClaims;
}


export { projectClaims, userClaims }