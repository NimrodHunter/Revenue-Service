import { ethers } from "ethers";
import  { provider } from '../utils/provider';
import  projects from '../utils/projects';
import nftAbi from '../abis/NFT.json';

import { NFT, ProjectNFTs } from '../types/nft';

const nftData = async (project: string, id: number) => {
    let nft: NFT | Error = {
        id: "0",
        createAt: "0",
        locked: "0",
        amount: "0"
    };
    const nftContract = new ethers.Contract(project, nftAbi, provider);
    try {
        const owner = await nftContract.ownerOf(id);
        const rawNFT = await nftContract.rsToken(id);
        nft = {
            "owner": owner,
            "id": id.toString(),
            "createAt": rawNFT[0].toString(),
            "locked": rawNFT[1].toString(),
            "amount": rawNFT[2].toString()
        }
    } catch (e) {
        if (e instanceof Error) {
            console.log(e)
            nft = e;
        }   
    }
    return nft;
}

const addressNFTs = async (address: string): Promise<ProjectNFTs[] | Error> => {
    const notFound = new Error('not found');
    let nfts: ProjectNFTs[] | Error = [];
    await Promise.all(projects.map(async (project: string | undefined) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const nftContract = new ethers.Contract(project!, nftAbi, provider);
        try {
            const balance = await nftContract.balanceOf(address);
            if (balance.toNumber() > 0) {
                let ids = await nftContract.rsId();
                ids = ids.toNumber();
                for (let i = 1; i <= ids; i++) {
                    const owner = await nftContract.ownerOf(i);
                    if (owner.localeCompare(address) === 0) {
                        let nft = await nftContract.rsToken(i);
                        nft = {
                            "id": i.toString(),
                            "createAt": nft[0].toString(),
                            "locked": nft[1].toString(),
                            "amount": nft[2].toString()
                        }
                        if (Array.isArray(nfts)) {
                            const pr = nfts.find(v => v.project === project)
                            if (pr) pr.nft.push(nft);
                            else {
                                const newProjectNFT: ProjectNFTs = {
                                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                                    "project": project!,
                                    "nft": [nft]
                                }
                                nfts.push(newProjectNFT)
                            }
                        }  
                    }
                }
            }
        } catch (e) {
            if (e instanceof Error) {
                console.log(e)
                nfts = e;
            }
        }
    }));
    if (Array.isArray(nfts) && nfts.length === 0) {
        nfts = notFound;
    }
    return nfts;
}

const projectNFTs = async (address: string) => {
    let blockNumber = 0;
    let timestamp = 0;
    const notFound = new Error('not found');
    let nfts: NFT[] | Error = [];
    const nftContract = new ethers.Contract(address, nftAbi, provider);
    try {
        let ids = await nftContract.rsId();
        ids = ids.toNumber();
        blockNumber = await provider.getBlockNumber()
        const block = await provider.getBlock(blockNumber);
        timestamp = block.timestamp;
        if (ids > 0) {
            for (let i = 1; i <= ids; i++) {
                const rawNFT = await nftContract.rsToken(i);
                const nft: NFT = {
                    "id": i.toString(),
                    "createAt": rawNFT[0].toString(),
                    "locked": rawNFT[1].toString(),
                    "amount": rawNFT[2].toString()
                };
                if (ethers.BigNumber.from(nft.createAt).lte(block.timestamp)) nfts.push(nft);
            }
        }
    } catch (e) {
        if (e instanceof Error) {
            console.log(e)
            nfts = e;
        }
    }
    if (Array.isArray(nfts) && nfts.length === 0) {
        nfts = notFound;
    }
    return { blockNumber, timestamp, nfts };
}

const projectNFTsByBlock = async (address: string, blockNumber: number) => {
    let timestamp = 0;
    const notFound = new Error('not found');
    let nfts: NFT[] | Error = [];
    const nftContract = new ethers.Contract(address, nftAbi, provider);
    try {
        let ids = await nftContract.rsId();
        ids = ids.toNumber();
        const block = await provider.getBlock(blockNumber);
        timestamp = block.timestamp;
        if (ids > 0) {
            for (let i = 1; i <= ids; i++) {
                const rawNFT = await nftContract.rsToken(i);
                const nft: NFT = {
                    "id": i.toString(),
                    "createAt": rawNFT[0].toString(),
                    "locked": rawNFT[1].toString(),
                    "amount": rawNFT[2].toString()
                };
                if (ethers.BigNumber.from(nft.createAt).lte(block.timestamp)) nfts.push(nft);
            }
        }
    } catch (e) {
        if (e instanceof Error) {
            console.log(e)
            nfts = e;
        }
    }
    if (Array.isArray(nfts) && nfts.length === 0) {
        nfts = notFound;
    }
    return { blockNumber, timestamp, nfts };
}

export { addressNFTs, projectNFTs, projectNFTsByBlock, nftData }