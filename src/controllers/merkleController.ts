import { Request, Response } from 'express';

import { ethers, utils, BigNumber as BN } from "ethers";
import { toChecksum } from '../utils/toChecksum';
import  projects from '../utils/projects';


import { projectNFTs, projectNFTsByBlock } from '../blockchain_data/nfts';
import  { MerkleTree } from 'merkletreejs';
import { NFT, BlockNFTs } from '../types/nft';


const addLoyalAmounts = (nfts: NFT[]) => {
    if (nfts.length === 1) {
        nfts[0].reward = '1000000';
        return nfts;
    }
    nfts.sort((a, b) => {
        return BN.from(a.locked).sub(b.locked).toNumber()
    })

    let totalAmount = BN.from("0");
    let totalLocked = BN.from("0");

    nfts.map((nft) => {
        totalAmount = totalAmount.add(nft.amount);
        totalLocked = totalLocked.add(nft.locked);
    });

    nfts.map((nft) => {
        const amountPiece = BN.from(nft.amount).mul(1000000).div(totalAmount);
        const lockedPiece = BN.from(nft.locked).mul(1000000).div(totalLocked);
        nft.reward = amountPiece.mul(75).div(100).add(lockedPiece.mul(25).div(100)).toString()
    });

    return nfts;
}

const createMerkleTree = (blockNft: BlockNFTs) => {
    if (!(blockNft.nfts instanceof Error)) {
        const leaves = blockNft.nfts.map((nft) => {
            return utils.solidityKeccak256(["uint256", "uint256"], [nft.id, nft.reward]);
        })
        const tree = new MerkleTree(leaves, utils.keccak256, { sort: true });
        return tree.getHexRoot();
    }
}


const mProof = (blockNft: BlockNFTs, id: number) => {
    if (!(blockNft.nfts instanceof Error)) {
        const nft = blockNft.nfts.find(nft => nft.id.localeCompare(id.toString()) === 0);
        if(typeof nft === 'undefined') return { reward: 0, leaf: 0, proof:0 }
        const leaf = utils.solidityKeccak256(["uint256", "uint256"], [nft.id, nft.reward]);
        const leaves = blockNft.nfts.map((nft) => {
            return utils.solidityKeccak256(["uint256", "uint256"], [nft.id, nft.reward]);
        })
        const tree = new MerkleTree(leaves, utils.keccak256, { sort: true });
        const proof = tree.getHexProof(leaf)

        return { reward: nft.reward, leaf, proof };
    }
}

const merkleByProject = async (req: Request, res: Response) => {
    const project = toChecksum(req.params.projectAddress);
    const validProject = projects.find(pr => pr?.localeCompare(project) === 0);
    if (!ethers.utils.isAddress(project) || validProject === undefined) {
        res.status(400).send({ error: 'invalid project address' });
    } else {
        const blockNfts = await projectNFTs(project);
        if (blockNfts.nfts instanceof Error) res.status(400).send({ error: "provider error...try again" });
        else {
            if (!blockNfts.nfts.length) res.status(500).send({ error: 'project has not nfts minted yet' });
            else {
                blockNfts.nfts = addLoyalAmounts(blockNfts.nfts);
                const tree = createMerkleTree(blockNfts);
                res.status(200).json({ "block":blockNfts.blockNumber, "timestamp":blockNfts.timestamp, "root":tree });
            }
        }
    }
}

const merkleByProjectBlock = async (req: Request, res: Response) => {
    const project = toChecksum(req.params.projectAddress);
    const validProject = projects.find(pr => pr?.localeCompare(project) === 0);
    if (!ethers.utils.isAddress(project) || validProject === undefined) {
        res.status(400).send({ error: 'invalid project address' });
    } else {
        const block = parseInt(req.params.block)
        const blockNfts = await projectNFTsByBlock(project, block);
        if (blockNfts.nfts instanceof Error) res.status(500).send({ error: "provider error...try again" });
        else {
            if (!blockNfts.nfts.length) res.status(400).send({ error: 'project has not nfts minted yet' });
            else {
                blockNfts.nfts = addLoyalAmounts(blockNfts.nfts);
                const tree = createMerkleTree(blockNfts);
                res.status(200).json({ "block":blockNfts.blockNumber, "timestamp":blockNfts.timestamp, "root":tree });
            }
        }
    }
}

const merkleProof = async (req: Request, res: Response) => {
    const project = toChecksum(req.params.projectAddress);
    const validProject = projects.find(pr => pr?.localeCompare(project) === 0);
    if (!ethers.utils.isAddress(project) || validProject === undefined) {
        res.status(400).send({ error: 'invalid project address' });
    } else {
        const block = parseInt(req.params.block);
        const id = parseInt(req.params.id);
        const blockNfts = await projectNFTsByBlock(project, block);
        if (blockNfts.nfts instanceof Error) res.status(500).send({ error: "provider error...try again" });
        else if (!blockNfts.nfts.length) res.status(400).send({ error: 'project has not nfts minted yet' });
        else {
            blockNfts.nfts = addLoyalAmounts(blockNfts.nfts);
            if (blockNfts.nfts.length === 0) res.status(400).send({ error: "invalid NFT id" });
            else {
                const result = mProof(blockNfts, id);
                if (result?.reward === 0) res.status(400).send({ error: "invalid NFT id" });
                else res.status(200).json({ "nftProof": result });
            }
        }
    }
}

export { merkleByProject, merkleByProjectBlock, merkleProof };

