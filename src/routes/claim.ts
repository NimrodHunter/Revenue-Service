import express from 'express';
import { claimByUser, claimById } from '../controllers/claimController';


const router = express.Router();

router.get('/claim/:userAddress', claimByUser);
router.get('/claim/:projectAddress/:nftId', claimById);

export = router;