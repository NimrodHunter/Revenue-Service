import express from 'express';
import { merkleByProject, merkleByProjectBlock, merkleProof } from '../controllers/merkleController';

const router = express.Router();

router.get('/merkle/:projectAddress', merkleByProject);
router.get('/merkle/:projectAddress/:block', merkleByProjectBlock);
router.get('/merkle/:projectAddress/:block/:id', merkleProof);

export = router;