import express from 'express';
import { nftByUser, nftByProject, nftDataController } from '../controllers/nftController';


const router = express.Router();

router.get('/user/:userAddress', nftByUser);
router.get('/project/:projectAddress', nftByProject);
router.get('/nft/:projectAddress/:id', nftDataController);

export = router;