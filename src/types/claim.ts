export interface Token {
    address: string,
    symbol: string,
    revenue: string
}

export interface Claim {
    contract: string,
    blockNumber: number,
    token: Token
}

export interface NFTClaims {
    project: string,
    nftId: string,
    revenues: Claim[]
}