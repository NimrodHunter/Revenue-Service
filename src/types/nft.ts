export interface NFT {
    id: string,
    owner?: string,
    createAt: string,
    locked: string,
    amount: string,
    reward?: string
}

export interface ProjectNFTs {
    project: string,
    nft: NFT[],
}

export interface BlockNFTs {
    blockNumber: number,
    timestamp: number,
    nfts: NFT[] | Error,
}