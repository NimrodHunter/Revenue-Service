import 'dotenv/config';
import { providers } from "ethers";

export const provider = new providers.InfuraProvider(process.env.NETWORK, process.env.INFURA_API_KEY);